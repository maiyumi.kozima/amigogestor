@extends('condominio.layout')
@section('content')

<div class="container">
<table class="table table-bordered">
    <tr>
        <th>Numero</th>
        <th>Nome do condominio</th>
        <th width="300px">Ações</th>
    </tr>
    @foreach ($condominios as $condominio)
    <tr>
        <td>{{ $condominio->id }}</td>
        <td>{{ $condominio->nomeFantasia }}</td>        
            <td>
                <form action="{{ route('condominios.destroy',$condominio->id) }}" method="POST">
                    <a class="btn btn-info" href="{{ route('condominios.show',$condominio->id) }}">Mostrar</a>
                    <a class="btn btn-primary" href="{{ route('condominios.edit',$condominio->id) }}">Editar</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Deletar</button>
                </form>
            </td>
        </tr>
        @endforeach
</table>  
</div>










@endsection
