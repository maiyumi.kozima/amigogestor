<?php

namespace App\Http\Controllers;

use App\Condominio;
use Illuminate\Http\Request;

class CondominioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $condominios = Condominio::all();

        return view('condominio.index', compact('condominios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('condominio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'razaoSolcial'  => 'required',
            'nomeFantasia'  => 'required',
            'cnpj'          => 'required',
            'longradouto'   => 'required',
            'bairro'        => 'required',
            'cidade'        => 'required',
            'estado'        => 'required'

        ]);

       Condominio::create($request->all());

        return redirect()->route('condominios.index')->with('success','Atualizado com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Condominio  $condominio
     * @return \Illuminate\Http\Response
     */
    public function show(Condominio $condominio)
    {
        return view('condominio.show',compact('condominio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Condominio  $condominio
     * @return \Illuminate\Http\Response
     */
    public function edit(Condominio $condominio)
    {
         return view('condominio.edit',compact('condominio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Condominio  $condominio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Condominio $condominio)
    {
        $request->validate([
            'razaoSolcial'  => 'required',
            'nomeFantasia'  => 'required',
            'cnpj'          => 'required',
            'longradouto'   => 'required',
            'bairro'        => 'required',
            'cidade'        => 'required',
            'estado'        => 'required'

        ]);

       $condominio->update($request->all());

        return redirect()->route('condominios.index')->with('success','Atualizado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Condominio  $condominio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Condominio $condominio)
    {
       $condominio->delete();

       return redirect()->route('condominios.index')->with('success','Condominio deletado com sucesso');
    }
}
