<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class HomeController extends Controller
{
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::all();

        return view('user.create', compact('usuarios'));
    }

    public function store(Request $request)
    {
         //validaçõ da request
         $request->validate([
            'name'  		=> 'required',
            'email' 	 	=> 'required',
            'password'      => 'required',
        ]);
         //Dando ao valor request o Hash dentro da variavel request
        $request['password'] = Hash::make($request['password']);
        User::create($request->all());

        return redirect()->route('condominios.index')->with('success','Atualizado com sucesso');
    }
}
