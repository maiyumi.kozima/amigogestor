<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'name' => 'renata',
            'email' => 'renata@gmail.com',
            'password' => bcrypt('renata'),
        ]);

        DB::table('users')->insert([
            'name' => 'nathalia',
            'email' => 'nathalia@gmail.com',
            'password' => bcrypt('nathalia'),
        ]);
    }
}
